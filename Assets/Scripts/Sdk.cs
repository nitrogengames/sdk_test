﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

[RequireComponent(typeof( AppsFlyerTrackerCallbacks) )]
public class Sdk : MonoBehaviour
{
    
    [Header("AppsFlyerFB SDK")]
    public string AppsflyerDevKey;
    public string iosAppId;


    protected string Status
    {
        get
        {
            return this.status;
        }

        set
        {
            this.status = value;
        }
    }
    private string status = "Ready";

    protected string LastResponse
    {
        get
        {
            return this.lastResponse;
        }

        set
        {
            this.lastResponse = value;
        }
    }
    private string lastResponse = string.Empty;

    protected Texture2D LastResponseTexture { get; set; }

    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(this.OnInitComplete, this.OnHideUnity);
            Debug.Log("FB.Init() called with " + FB.AppId);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    void Start()
    {
        

        #region APPSFLYER
        if (AppsflyerDevKey != null)
        {
            /* Mandatory - set your AppsFlyer’s Developer key. */
            AppsFlyer.setAppsFlyerKey(AppsflyerDevKey);
            /* For detailed logging */
            /* AppsFlyer.setIsDebug (true); */

#if UNITY_IOS
   
      /* Mandatory - set your apple app ID
      NOTE: You should enter the number only and not the "ID" prefix */
      AppsFlyer.setAppID (iosAppId);
      AppsFlyer.getConversionData();
      AppsFlyer.trackAppLaunch ();
      
#elif UNITY_ANDROID

            /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
            AppsFlyer.init(AppsflyerDevKey, "AppsFlyerTrackerCallbacks");

#endif
        }
        #endregion
    }

    private void CallFBLogin()
    {
        FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleResult);
    }

    private void CallFBLoginForPublish()
    {
        // It is generally good behavior to split asking for read and publish
        // permissions rather than ask for them all at once.
        //
        // In your own game, consider postponing this call until the moment
        // you actually need it.
        FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, this.HandleResult);
    }

    private void CallFBLogout()
    {
        FB.LogOut();
    }

    private void OnInitComplete()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }

        this.Status = "Success - Check log for details";
        this.LastResponse = "Success Response: OnInitComplete Called\n";
        string logMessage = string.Format(
            "OnInitCompleteCalled IsLoggedIn='{0}' IsInitialized='{1}'",
            FB.IsLoggedIn,
            FB.IsInitialized);
        Debug.Log((logMessage));
        if (AccessToken.CurrentAccessToken != null)
        {
            Debug.Log(AccessToken.CurrentAccessToken.ToString());
        }


    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }

        this.Status = "Success - Check log for details";
        this.LastResponse = string.Format("Success Response: OnHideUnity Called {0}\n", isGameShown);
        Debug.Log("Is game shown: " + isGameShown);
    }

    protected void HandleResult(IResult result)
    {
        if (result == null)
        {
            this.LastResponse = "Null Response\n";
            Debug.Log(this.LastResponse);
            return;
        }

        this.LastResponseTexture = null;

        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
            this.Status = "Error - Check log for details";
            this.LastResponse = "Error Response:\n" + result.Error;
        }
        else if (result.Cancelled)
        {
            this.Status = "Cancelled - Check log for details";
            this.LastResponse = "Cancelled Response:\n" + result.RawResult;
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            this.Status = "Success - Check log for details";
            this.LastResponse = "Success Response:\n" + result.RawResult;
        }
        else
        {
            this.LastResponse = "Empty Response\n";
        }

        Debug.Log(result.ToString());
    }

    public void CallEvent()
    {
        this.Status = "Logged FB.AppEvent";
        FB.LogAppEvent(
            AppEventName.UnlockedAchievement,
            null,
            new Dictionary<string, object>()
            {
                        { AppEventParameterName.Description, "Clicked 'Log AppEvent' button" }
            });
        Debug.Log((
            "You may see results showing up at https://www.facebook.com/analytics/"
            + FB.AppId));
    }

    public void SendValue()
    {
        this.Status = "Logged FB.AppEvent";
        FB.LogAppEvent(
            AppEventName.AchievedLevel,
            0.75f,
            new Dictionary<string, object>()
            {
                        { AppEventParameterName.Level, Random.Range(0,10).ToString() }
            });
        Debug.Log((
            "You may see results showing up at https://www.facebook.com/analytics/"
            + FB.AppId));
    }

}
